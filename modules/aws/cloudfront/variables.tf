variable "origin_access_identity_comment" {}
variable "api_deployment" {}
variable "apigw" {}
variable "cf_enabled" {}
variable "restriction_type" {}
variable "restrict_locations" {}
variable "allowed_methods" {}
variable "cached_methods" {}
