resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.origin_access_identity_comment}"
}

resource "aws_cloudfront_distribution" "apigateway_distribution" {
  origin {
    domain_name = replace("${var.api_deployment}", "/^https?://([^/]*).*/", "$1")
    origin_id   = "${var.apigw}"
    origin_path = "/prod"
    
    custom_origin_config {
		http_port              = 80
		https_port             = 443
		origin_protocol_policy = "https-only"
		origin_ssl_protocols   = ["TLSv1.2"]
	}
  }
  enabled             = "${var.cf_enabled}"
  default_cache_behavior {
	allowed_methods  = "${var.allowed_methods}"
	cached_methods   = "${var.cached_methods}"
	target_origin_id = "${var.apigw}"

	default_ttl = 0
	min_ttl     = 0
	max_ttl     = 0

	forwarded_values {
		query_string = true
		cookies {
			forward = "all"
		}
	}

	viewer_protocol_policy = "redirect-to-https"
  }
  restrictions {
    geo_restriction {
      restriction_type = "${var.restriction_type}"
      locations        = "${var.restrict_locations}"
    }
  }


  viewer_certificate {
    cloudfront_default_certificate = true
  }
}


 
