resource "aws_s3_bucket" "mybucket" {
   bucket = "${var.bucket}"
   acl = "${var.acl}"
   lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days = 60
      storage_class = "GLACIER"
    }
  }
}
