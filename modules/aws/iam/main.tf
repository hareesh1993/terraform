resource "aws_iam_user" "test" {
  name = "${var.iam_user}"
  path = "/system/"

}

resource "aws_iam_access_key" "testkey" {
  user = "${aws_iam_user.test.name}"
}

resource "aws_iam_user_policy" "testpolicy" {
  name = "${var.policy_name}"
  user = "${aws_iam_user.test.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
