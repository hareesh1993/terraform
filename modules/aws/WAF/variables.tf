variable "ipset_name" {}
variable "ipset_type" {}
variable "ipset" {}
variable "wafrule_name" {}
variable "wafacl_name" {}
variable "waf_default_action" {}
variable "waf_rule_action" {}
variable "wafrule_type" {}
