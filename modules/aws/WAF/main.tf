resource "aws_waf_ipset" "ipset" {
  name = "${var.ipset_name}"

  ip_set_descriptors {
    type  = "${var.ipset_type}"
    value = "${var.ipset}"
  }
}

resource "aws_waf_rule" "wafrule" {
  depends_on  = ["aws_waf_ipset.ipset"]
  name        = "${var.wafrule_name}"
  metric_name = "${var.wafrule_name}"

  predicates {
    data_id = "${aws_waf_ipset.ipset.id}"
    negated = false
    type    = "IPMatch"
  }
}

resource "aws_waf_web_acl" "waf_acl" {
  depends_on  = ["aws_waf_ipset.ipset", "aws_waf_rule.wafrule"]
  name        = "${var.wafacl_name}"
  metric_name = "${var.wafacl_name}"

  default_action {
    type = "${var.waf_default_action}"
  }

  rules {
    action {
      type = "${var.waf_rule_action}"
    }

    priority = 1
    rule_id  = "${aws_waf_rule.wafrule.id}"
    type     = "${var.wafrule_type}"
  }
}
