output "lambda_arn" {
  description = "The arn of the lambda"
  value       = "${aws_lambda_function.pim_lambda.arn}"
}

output "lambda_invoke_arn" {
  description = "The invoke_arn of the lambda"
  value       = "${aws_lambda_function.pim_lambda.invoke_arn}"
}
output "lambda_function_name" {
  description = "The invoke_arn of the lambda"
  value       = "${aws_lambda_function.pim_lambda.function_name}"
}

