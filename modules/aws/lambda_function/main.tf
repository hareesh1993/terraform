resource "aws_iam_role" "pim_iam_for_lambda" {
  name = "${var.iam_role_lambda}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "pim_lambda" {
  filename      = "${var.filename}"
  function_name = "${var.lambda_function_name}"
  role          = "${aws_iam_role.pim_iam_for_lambda.arn}"
  handler       = "${var.handler}"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("${var.filename}"))}"
  source_code_hash = "${filebase64sha256("${var.filename}")}"

  runtime = "${var.runtime}"

}


