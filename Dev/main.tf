provider "aws" {
  region     = "${var.region}"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}
module "cloudfront" {
  source		= "../modules/aws/cloudfront"
  origin_access_identity_comment = "${var.cf_oai}"
  api_deployment	= "${module.api_gateway.deployment_invoke_url}"
  apigw			= "${module.api_gateway.id}"
  cf_enabled		= "${var.cf_enabled}"
  restriction_type 	= "${var.restriction_type}"
  restrict_locations   = "${var.restrict_locations}"
  allowed_methods  = "${var.allowed_methods}"
  cached_methods   = "${var.cached_methods}"

}
module "api_gateway" {
  source 			= "../modules/aws/apigateway"
  authorizer_name	= "${var.authorizer_name}"
  authorizer_uri	= "${module.lambda.lambda_invoke_arn}"
  apigateway_role	= "${var.apigateway_role}"
  apigateway_policy	= "${var.apigateway_policy}"
  resource_apigateway	= "${module.lambda.lambda_arn}"
  rest_api_name 	= "${var.rest_api_name}"
  stage_name 		= "${var.stage_name}"
  resource_path 	= "${var.resource_path}"
  http_method 		= "${var.http_method}"
  authorization		= "${var.authorization}"
  integration_type	= "${var.integration_type}"
  api_key_name		= "${var.api_key_name}"
  usage_plan_name	= "${var.usage_plan_name}"
  product_code		= "${var.product_code}"
  quota_limit		= "${var.quota_limit}"
  quota_offset		= "${var.quota_offset}"
  quota_period		= "${var.quota_period}"
  burst_limit		= "${var.burst_limit}"
  rate_limit		= "${var.rate_limit}"
  key_type		= "${var.key_type}"
  lambda_function_name  = "${module.lambda.lambda_function_name}"
  status_code		= "${var.status_code}" 
  region		= "${var.region}"
  accountId		= "${var.accountId}"
  resource2_path	= "${var.resource_path2}"
  http_method2		= "${var.http_method2}"
  lambda_function_name2 = "${module.lambda2.lambda_function_name}"
  authorizer_uri2	= "${module.lambda2.lambda_invoke_arn}"
}

module "lambda" {
  source 			= "../modules/aws/lambda_function"
  iam_role_lambda	= "${var.iam_role_lambda}"
  filename			= "${var.filename}"
  lambda_function_name = "${var.lambda_function_name}"
  handler			= "${var.handler}"
  runtime			= "${var.runtime}"
  
}

module "lambda2" {
  source                        = "../modules/aws/lambda_function"
  iam_role_lambda       = "${var.iam_role_lambda2}"
  filename                      = "${var.filename2}"
  lambda_function_name = "${var.lambda_function_name2}"
  handler                       = "${var.handler}"
  runtime                       = "${var.runtime}"

}

