	variable "access_key" {
   default = "AKIA3X5S3LMJ7JH4HEVM"
}
variable "secret_key"{
   default = "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"
}
variable "region" {
   default = "us-west-1"
}
variable "accountId" {
   type = "string"
   default = "807292066579"
}
variable "bucket" {
  type = "string"
  description = "name of bucket"
#  default = "harishs3"
}
variable "acl" {
  type = "string"
  description = "how the bucket access would be"
#  default = "public"
}
variable "cidr" {
   default = "10.0.0.0/16"
}
variable "ami_id" {
   default = "ami-05b6753ece63a308a"
}
variable "size" {
   default = "t2.micro"
}
variable "tag" {
   default = "myfirstvm"
}
variable "iam_user" {
  default = "myfirstuser"
}
variable "policy_name" {
  default = "myuserpolicy"
}
variable "bucket_cf" {
  type = "string"
  description = "name of bucket"
#  default = "harishs3"
}
variable "acl_cf" {
  type = "string"
  description = "how the bucket access would be"
#  default = "public"
}

variable "ipset_name" {
  type = "string"
  description = "name of the ipset"
}
variable "ipset_type" {
  type = "string"
  description = "type of ipset"
}
variable "ipset" {
  type = "string"
  description = "range of ipaddresses"
}
variable "wafrule_name" {
  type = "string"
  description = "rule name of waf"
}
variable "wafacl_name" {
  type = "string"
  description = "acl name of acl"
}
variable "waf_default_action" {
  type = "string"
  description = "default action to take when no rules match"
}
variable "waf_rule_action" {
  type = "string"
  description = "action to take when specific rules match"
}
variable "wafrule_type" {
  type = "string"
  description = "type of wafrule"
}
variable "origin_access_identity_comment" {
  type = "string"
  description = "origin access identity"
}
variable "cf_enabled" {
  type = "string"
  description = "Do you want to enable cloudfront"
}
variable "ipv6_enabled" {
  type = "string"
  description = "Do you want to enable ipv6"
}
variable "cf_comment" {
  type = "string"
  description = "cloud front comment"
}
variable "default_root_object" {
  type = "string"
  description = "default root object once you hit cloud front domain name"
}
variable "allowed_methods" {
  type = "list"
  description = "cloudfront allowed methods"
}
variable "cached_methods" {
  type = "list"
  description = "cloudfront cache methods"
}
variable "restriction_type" {
  type = "string"
  description = "cloud front access restrictions"
}
variable "restrict_locations" {
  type = "list"
  description = "locations you want to restrict to access cloudfront domain"
}
## API_GATEWAY

variable "authorizer_name" {
  type        = "string"
  description = "name of authorizer"
  default     = "api_authorizer"
}

variable "apigateway_role" {
  type        = "string"
  description = "iam role for apigateway"
  default     = "apigateway_role"
}

variable "apigateway_policy" {
  type        = "string"
  description = "policy for apigateway to invoke lambda"
  default     = "apigateway_policy"
}


variable "rest_api_name" {
  type        = "string"
  description = "name of restapi"
  default     = "test-rest-api"
}
variable "stage_name" {
  type        = "string"
  description = "stage name of api"
  default     = "prod"
}
variable "resource_path" {
  type        = "string"
  description = "path of the resource"
  default     = "forgot_password"
}
variable "http_method" {
  type        = "string"
  description = "method of http"
  default     = "POST"
}
variable "authorization" {
  type        = "string"
  description = "autherization enabled or not enabled"
  default     = "NONE"
}
variable "integration_type" {
  type        = "string"
  description = "type of integration"
  default     = "AWS_PROXY"
}
variable "api_key_name" {
  type        = "string"
  description = "name of api key"
  default     = "test_key"
}
variable "usage_plan_name" {
  type        = "string"
  description = "name of usage plan"
  default     = "test_up"
}
variable "product_code" {
  type        = "string"
  description = "The AWS Markeplace product identifier to associate with the usage plan as a SaaS product on AWS Marketplace"
  default     = "test_pc"
}
variable "quota_limit" {
  type        = "string"
  description = "The maximum number of requests that can be made in a given time period"
  default     = "122"
}
variable "quota_offset" {
  type        = "string"
  description = "The number of requests subtracted from the given limit in the initial time period"
  default     = "1"
}
variable "quota_period" {
  type        = "string"
  description = "The time period in which the limit applies. Valid values are DAY, WEEK or MONTH."
  default     = "MONTH"
}
variable "burst_limit" {
  type        = "string"
  description = "The API request burst limit, the maximum rate limit over a time ranging from one to a few seconds"
  default     = "100"
}
variable "rate_limit" {
  type        = "string"
  description = "The API request steady-state rate limit."
  default     = "100"
}
variable "key_type" {
  type        = "string"
  description = "The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "API_KEY"
}
variable "status_code" {
  type        = "string"
  description = "status code of method"
  default     = "200"
}
variable "resource_path2" {
  type        = "string"
  description = "path of the resource"
  default     = "login_page"
}
variable "http_method2" {
  type        = "string"
  description = "method of http"
  default     = "POST"
}


## LAMBDA

variable "iam_role_lambda" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "iam_role_lambda"
}
variable "filename" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "index.zip"
}
variable "lambda_function_name" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "test_lambda"
}
variable "handler" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "index.handler"
}
variable "runtime" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "nodejs12.x"
}

variable "filename2" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "index1.zip"
}


variable "lambda_function_name2" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "order_lambda"
}

variable "iam_role_lambda2" {
  type        = "string"
  description = " The type of the API key resource. Currently, the valid key type is API_KEY"
  default     = "iam_role_lambda2"
}

variable "cf_oai" {
  type = "string"
  default = "test_OAI"
 }
